# colab.google.com

Проект лежит в открытом доступе на colab-е. [Вот тут](https://colab.research.google.com/drive/1qmR0aLemBbx7A9M0TIB3AOKUDYr6YcVh?usp=sharing). По этой ссылке его можно редактировать. 

Ctrl + Enter - чтобы запустить ячейку. Ячейка там всего одна, так что можно просто нажать на стрелочку в крге в левом верхнем углу.

# запуск
```bash
python3.8 -m pip install -r requirements.txt
python3.8 main.py repl

> run_government -e 0.01 -d 0.9 0.05 0.05
...
```
