import typing as t
import numpy as np

from solution.sir.odesolver import EvaluationResult


class GradientSolver:
    function: t.Callable[[np.ndarray], float]

    def __init__(self, evaluate_function: t.Callable[[np.ndarray], float]):
        self.function = evaluate_function

    def gradient(self, x0: np.ndarray) -> np.ndarray:
        eps = np.finfo(np.double).eps
        g = []
        for i in range(len(x0)):
            h = x0[i] * np.sqrt(eps)
            if h == 0:
                h = eps

            lx = np.array(x0)
            lx[i] += h

            rx = np.array(x0)
            rx[i] -= h

            diff = (float(self.function(lx)) - float(self.function(rx)))
            d = diff / (2 * h)

            g.append(d)
        return np.array(g)
