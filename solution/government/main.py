import typing as t
import numpy as np
from scipy.optimize import OptimizeResult

from solution.government.solvers.gov_ode_solver import \
    ODESolver as GovernmentODESolver


def main_government(
        sir: t.Optional[np.ndarray] = None,
        SIR: t.Optional[np.ndarray] = None,
        e: t.Optional[np.ndarray] = None
):
    sir = sir if sir is not None else np.array([0.95, 0.049, 0.001])
    SIR = SIR if SIR is not None else np.array([0, 0, 0], dtype=np.double)
    e = e if e is not None else np.array([0.1], dtype=np.double)

    print(f"start distribution: {sir}")
    print(f"(start optimisation player gain): {SIR}")
    print(f"(start optimisation government threshold): {e}")

    gov_solver = GovernmentODESolver(sir, SIR)
    gov_result: OptimizeResult = gov_solver.optimize(e)
    opt_e = gov_result.x

    evaluation_result = gov_solver.evaluate(opt_e)
    opt_q = evaluation_result

    print(f"opt q: {opt_q}")
    print(f"opt e: {opt_e}")
    print(f"final distribution: {evaluation_result.y[-1][:3]}")
    print(f"final gain: {evaluation_result.y[-1][3:6]}")
