from dataclasses import field

import numpy as np
from scipy.integrate import odeint
from scipy.optimize import minimize, OptimizeResult

from solution.government.dif import dif_j, dif_theta
from solution.gradient import GradientSolver
from solution.government.model import model
from solution.government.constants import WEEKS, POINTS_NUMBER, c_1, beta_1


class ODESolver:
    disp: bool
    sir: np.ndarray
    e: np.ndarray

    def __init__(self, sir: np.ndarray, e: np.ndarray, *,
                 disp: bool = False) -> None:
        self.sir = sir
        self.e = e
        self.disp = disp

    def evaluate(self, SIR: np.ndarray) -> "EvaluationResult":
        time = np.linspace(0, WEEKS, num=POINTS_NUMBER)

        # # odeint не сходится и вообще жалуется...
        #
        # y = odeint(model, np.concatenate([self.sir, SIR, [1]]), time,
        #            args=(self.e[0],))

        # use hand written ode integration
        e = self.e[0]
        function = []
        y = np.concatenate([self.sir, SIR])
        for _ in time:
            if np.min(y[:3]) < 0:
                y[:3] -= np.min(y[:3])
                y[:3] /= np.sum(y[:3])
            s, i, r, Js, Ji, Jr = y
            maxarg = 0
            if c_1 + (Ji - Js) * beta_1 * i < 0 and i < e:
                maxarg = 1
            d_j = dif_j(e, maxarg, y)
            d_theta = dif_theta(maxarg, y)

            y[:3] += d_theta * WEEKS / POINTS_NUMBER
            y[:3] /= np.sum(y[:3])
            y[3:6] += -d_j * WEEKS / POINTS_NUMBER
            function.append(np.concatenate([y, [2 * maxarg - 1]]))

        function = np.array(function)

        assert_message: str = f"неправильное распределение: {function[-1][:3]}"
        assert abs(1 - np.sum(function[-1][:3])) < 0.00001, assert_message

        return EvaluationResult(function, self.e)

    def optimize(self, start_SIR: np.ndarray) -> OptimizeResult:
        gradient_solver = GradientSolver(self.evaluate)

        res: OptimizeResult = minimize(
            self.evaluate,
            start_SIR,
            method='BFGS',
            jac=gradient_solver.gradient,
            options={'disp': self.disp},
        )

        return res


class EvaluationResult(float):
    y: np.ndarray
    e: np.ndarray
    diff: np.double = field(init=False)

    def __new__(self, y: np.ndarray, e: np.ndarray):
        self.y = y
        self.e = e
        return float.__new__(self, np.sum(np.abs(self.y[-1][3:6]) ** 2))

    def __init__(self, y: np.ndarray, e: np.ndarray):
        float.__init__(np.sum(np.abs(self.y[-1][3:6]) ** 2))
        self.y = y
        self.e = e

    @property
    def u(self) -> np.ndarray:
        """
        control:
            0 - stay home
            1 - violate social distancing
        :return:
        """
        return np.array(self.y[:, 6] > 0, dtype=np.int)

    @property
    def Q(self) -> np.double:
        """
        Government loss: sum(s * c_1 * u) for every point and
        :return:
        """
        mask = self.y[::, 1] < self.e[0]
        u = self.u * mask
        return np.sum(self.y[:, 0] * u * c_1)
