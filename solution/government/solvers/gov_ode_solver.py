from dataclasses import field

import numpy as np
from scipy.optimize import minimize, OptimizeResult, approx_fprime

from solution.gradient import GradientSolver
from solution.government.constants import c_1
from solution.government.solvers.sir_ode_solver import \
    ODESolver as SIRODESolver


class ODESolver:
    sir: np.ndarray
    SIR: np.ndarray

    def __init__(self, sir: np.ndarray, SIR: np.ndarray) -> None:
        self.sir = sir
        self.SIR = SIR

    def evaluate(self, e: np.ndarray) -> "EvaluationResult":
        if np.max(e) < 0:
            # штрафуем наш минимизатор, когда он неверные аргументы
            return 1000
        ode_solver = SIRODESolver(self.sir, e)
        optimize_result = ode_solver.optimize(self.SIR)

        optimal_SIR = optimize_result.x
        sir_evaluation_result = ode_solver.evaluate(optimal_SIR)

        return EvaluationResult(sir_evaluation_result.y)

    def optimize(self, start_e: np.ndarray) -> "OptimizeResult":
        gradient_solver = GradientSolver(self.evaluate)

        res: OptimizeResult = minimize(
            self.evaluate,
            start_e,
            method='BFGS',
            jac=gradient_solver.gradient,
            options={
                # 'disp': True,
                'maxiter': 5
            }
        )

        return res


class EvaluationResult(float):
    y: np.ndarray
    diff: np.double = field(init=False)

    def __new__(self, y: np.ndarray):
        self.y = y
        q = np.sum(
            self.y[:, 0] * np.array(self.y[:, 6] > 0, dtype=np.int) * c_1)
        return float.__new__(self, q)

    def __init__(self, y: np.ndarray):
        q = np.sum(
            self.y[:, 0] * np.array(self.y[:, 6] > 0, dtype=np.int) * c_1)
        float.__init__(q)
        self.y = y

    def u(self) -> np.ndarray:
        """
        control:
            0 - stay home
            1 - violate social distancing
        :return:
        """
        return np.array(self.y[:, 6] > 0, dtype=np.int)
