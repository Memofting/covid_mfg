import numpy as np

from solution.government.constants import beta_0, beta_1, gamma, cost


def dif_theta(maxarg, y) -> np.ndarray:
    s, i, r, Js, Ji, Jr, *_ = y
    return np.array([
        -(beta_0 + maxarg * beta_1) * i * s,
        (beta_0 + maxarg * beta_1) * i * s - gamma * i,
        gamma * i
    ], dtype=float)


def dif_j(e: np.double, maxarg: int, y: np.ndarray) -> np.ndarray:
    s, i, r, Js, Ji, Jr, *_ = y
    res = np.array(cost(i, e)[maxarg])
    res[0] += (Ji - Js) * (beta_0 + maxarg * beta_1) * i
    res[1] += (Jr - Ji) * gamma
    return res
