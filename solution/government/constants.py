import typing as t
from functools import partial

import numpy as np

# WEEKS - количество рассматриваемых недель
# POINTS_NUMBER - количество временных промежутков,
#                 по которым мы строим свои рассчёты
MONTHS = 1
WEEKS = MONTHS * 4
HOURS = 7 * 24
MULTIPLIER = 1 * HOURS
POINTS_NUMBER = WEEKS * MULTIPLIER

# ускоряем gamma для более интересной симуляции
beta_0, beta_1, gamma = 0.152, 0.12, 0.05
# Отрицательнные - потому что мы минимизируем. Даны в десяти тысячах.
# c_0 = МРОТ
# c_1 = средняя ЗП - c_0
# с_2 = лечение COVID умноженное на количество симптомных
c_0, c_1, c_2 = -1.1, -2.7, -0.7


def get_cost(c_0, c_1, c_2, i: np.double, e: np.double):
    return np.array([
        [c_0, -c_2, c_0 + c_1],
        [c_0 + (i < e) * c_1, -c_2, c_0 + c_1]
    ], dtype=float)


cost = partial(get_cost, c_0, c_1, c_2)
