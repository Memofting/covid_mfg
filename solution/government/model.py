import typing as t
import numpy as np
from solution.government.constants import beta_1, c_1
from solution.government.dif import dif_theta, dif_j


def model(
        y: np.ndarray,
        time: float,
        e: np.double
) -> np.ndarray:
    '''

    :param y: (s, i, r, Js, Ji, Jr, maxarg)
    :param time: time point
    :param e: government threshold for infected,
              it's prevent paying for staying home
    :return: dy/dt
    '''
    s, i, r, Js, Ji, Jr, *_ = y
    maxarg = 0
    if c_1 + (Ji - Js) * beta_1 * i < 0 and i < e:
        maxarg = 1
    d_j = dif_j(e, maxarg, y)
    d_theta = dif_theta(maxarg, y)

    ny = np.zeros((7,))
    ny[:3] = d_theta
    ny[3:6] = -d_j
    ny[6] = 2 * maxarg - 1
    return ny
