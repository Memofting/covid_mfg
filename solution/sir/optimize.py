import numpy as np

from solution.sir.odesolver import ODESolver
from solution.gradient import GradientSolver
from scipy.optimize import minimize, OptimizeResult


def optimize(sir: np.ndarray, SIR: np.ndarray) -> OptimizeResult:
    ode_solver = ODESolver(sir)
    gradient_solver = GradientSolver(ode_solver.evaluate)

    res: OptimizeResult = minimize(
        ode_solver.evaluate,
        SIR,
        method='BFGS',
        jac=gradient_solver.gradient,
        options={'disp': True}
    )

    return res
