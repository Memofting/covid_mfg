from dataclasses import dataclass, field

import numpy as np
from solution.sir.model import model
from scipy.integrate import odeint
from solution.sir.constants import WEEKS, POINTS_NUMBER, c_1


class ODESolver:
    sir: np.ndarray

    def __init__(self, sir: np.ndarray) -> None:
        self.sir = sir

    def evaluate(self, SIR: np.ndarray) -> "EvaluationResult":
        time = np.linspace(0, WEEKS, num=POINTS_NUMBER)

        y = odeint(model, np.concatenate([self.sir, SIR, [1]]), time)

        assert_message: str = f"неправильное распределение: {y[-1][:3]}"
        assert abs(1 - np.sum(y[-1][:3])) < 0.00001, assert_message

        return EvaluationResult(y)


class EvaluationResult(float):
    y: np.ndarray
    diff: np.double = field(init=False)

    def __new__(self, y: np.ndarray):
        self.y = y
        return float.__new__(self, np.sum(np.abs(self.y[-1][3:6]) ** 2))

    def __init__(self, y: np.ndarray):
        float.__init__(np.sum(np.abs(self.y[-1][3:6]) ** 2))
        self.y = y

    @property
    def u(self) -> np.ndarray:
        """
        control:
            0 - stay home
            1 - violate social distancing
        :return:
        """
        return np.array(self.y[:, 6] > 0, dtype=np.int)

    @property
    def Q(self) -> np.double:
        """
        Government loss: sum(s * c_1 * u) for every point and
        :return:
        """
        return np.sum(self.y[:, 0] * self.u * c_1)
