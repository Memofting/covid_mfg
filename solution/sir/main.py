import numpy as np
from scipy.optimize import OptimizeResult

from solution.sir.optimize import optimize as optimize_sir


def main_sir():
    sir = np.array([0.99, 0.01, 0])
    SIR = np.array([0, 0, 0], dtype=np.double)

    result: OptimizeResult = optimize_sir(sir, SIR)

    print(result)