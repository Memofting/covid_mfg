import numpy as np
from solution.sir.constants import beta_1, c_1
from solution.sir.dif import dif_theta, dif_j


def model(
        y: np.ndarray,
        time: float
) -> np.ndarray:
    '''

    :param y: (s, i, r, Js, Ji, Jr, maxarg)
    :param time: time point
    :return: dy/dt
    '''
    s, i, r, Js, Ji, Jr, *_ = y
    maxarg = 0
    if c_1 + (Ji - Js) * beta_1 * i < 0:
        maxarg = 1
    d_j = dif_j(maxarg, y)
    d_theta = dif_theta(maxarg, y)

    ny = np.zeros((7,))
    ny[:3] = d_theta
    ny[3:6] = -d_j
    ny[6] = 2 * maxarg - 1
    return ny
