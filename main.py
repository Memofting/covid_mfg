import typing as t
import io
from contextlib import redirect_stdout

import click
from click_repl import register_repl


@click.command()
@click.option('-d', '--distribution', default=[0.99, 0.01, 0], type=(float, float, float),
              help='Начальное распределение больных в населении.')
@click.option('-g', '--gain', default=[0, 0, 0], type=(float, float, float),
              help='Начальная точка, с которой начинается оптимизация выигрыша игрока.')
@click.option('-e', default=0.1, type=float,
              help='Краевое значение больных, после достижения которого '
                   'государство прекращает финансовую поддержку населения.')
def run_government(
        distribution: t.Tuple[float, float, float],
        gain: t.Tuple[float, float, float],
        e: float
) -> None:
    f = io.StringIO()
    with redirect_stdout(f):
        import numpy as np
        from solution.government.main import main_government
        main_government(
            np.array(distribution),
            np.array(gain),
            np.array([e]))
    s = f.getvalue()
    click.echo(s)


if __name__ == "__main__":
    cli = click.Group("cli", commands={
        "run_government": run_government,
    })

    register_repl(cli)
    cli()
    # main_sir()
    # main_government()
    # main_government_with_e(np.array([0.01], dtype=np.ndarray))
